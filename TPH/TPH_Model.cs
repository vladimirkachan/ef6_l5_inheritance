﻿using System;
using System.Data.Entity;
using System.Linq;

namespace TPH
{
    public class TPH_Model : DbContext
    {
        static TPH_Model()
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<TPH_Model>());
        }
        public TPH_Model()
            : base("name=TPH_Model")
        {
        }

        public virtual DbSet<Phone> Phones { get; set; }
        public virtual DbSet<SmartPhone> SmartPhones { get; set; }
    }
}