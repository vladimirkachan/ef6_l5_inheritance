﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPC;

namespace ConsoleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            //using TPH_Model db = new();
            //using TPT_Model db = new();
            using TPC_Model db = new();
            db.Phones.AddRange(new[]
            {
                new Phone { Name = "Samsung Galaxy S3", Company = "Samsung", Price = 5000 }, new Phone { Name = "Nokia Lumia 730", Company = "Nokia", Price = 3000 }
            });
            db.SaveChanges();
            db.SmartPhones.Add(new SmartPhone {Name = "iPhone 6", Company = "Apple", Price = 16000, OS = "iOS"});
            db.SmartPhones.Add(new SmartPhone { Name = "iPhone 5", Company = "Apple", Price = 10000, OS = "iOS" });
            db.SaveChanges();
            Console.WriteLine($"\tPhone count: {db.Phones.Count()}");
            foreach(var p in db.Phones) Console.WriteLine($"{p.Id}. {p.Name} [{p.Company}] ${p.Price}");
            Console.WriteLine($"\tSmartPhone count: {db.SmartPhones.Count()}");
            foreach(var p in db.SmartPhones) Console.WriteLine($"{p.Id}. {p.Name} [{p.Company}] ${p.Price} {p.OS}");
        }
    }
}
