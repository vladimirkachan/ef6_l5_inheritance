﻿using System;
using System.Data.Entity;
using System.Linq;

namespace TPC
{
    public class TPC_Model : DbContext
    {
        static TPC_Model()
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<TPC_Model>());
        }
        public TPC_Model()
            : base("name=TPC_Model")
        {
        }
        public virtual DbSet<Phone> Phones { get; set; }
        public virtual DbSet<SmartPhone> SmartPhones { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Phone>().Map(phone =>
            {
                phone.MapInheritedProperties();
                phone.ToTable("Phones");
            });
            modelBuilder.Entity<SmartPhone>().Map(phone =>
            {
                phone.MapInheritedProperties();
                phone.ToTable("SmartPhones");
            });
        }
    }
}