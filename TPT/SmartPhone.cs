﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPT
{
    [Table("SmartPhones")]
    public class SmartPhone : Phone
    {
        public string OS { get; set; }
    }
}
