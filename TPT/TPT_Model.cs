﻿using System;
using System.Data.Entity;
using System.Linq;

namespace TPT
{
    public class TPT_Model : DbContext
    {
        static TPT_Model()
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<TPT_Model>());
        }
        public TPT_Model()
            : base("name=TPT_Model")
        {
        }
        public virtual DbSet<Phone> Phones { get; set; }
        public virtual DbSet<SmartPhone> SmartPhones { get; set; }

    }
}